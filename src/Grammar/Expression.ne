# ***************************************
# * READ THIS BEFORE YOU CLICK ANYTHING *
# ***************************************

# When you open this file in VSCode, it may ask you to install an extension to
# help with .ne files. I do **not** recommend installing the extension that it
# recommends: based on my testing, the extension is incomplete, and in some
# cases involving parentheses it produces badly incorrect syntax highlighting
# (font colors). If you do choose to install the Nearley extension, be aware
# that the syntax highlighting in .ne files may sometimes be misleading.

# **********************
# * THANKS FOR READING *
# **********************


@preprocessor typescript

@{%
  // Within this block, we're writing TypeScript code, so we use TypeScript
  // comment syntax. This code is going to end up in the
  // gen/Grammar/Expression.ts file, so we use "../.." to navigate to the
  // project root in order to find the src folder. You should never modify the
  // files in the gen folder: they'll get overwritten each time you build this
  // project.
  import {
    AST,
    PlusNode, MinusNode, TimesNode, ExponentNode,
    NegateNode,
    NumLeaf, NameLeaf
  } from "../../src/AST";

  import { Token } from "moo";
  import { lexer } from "../../src/Grammar/Lexer";

  import { postprocessWith } from "../../src/Library/Parsing";

  import {
    unparenthesize,
    buildPlusNode, buildMinusNode, buildTimesNode, buildExponentNode,
    buildNegateNode,
    buildNumLeaf, buildNameLeaf
  } from "../../src/Grammar/Postprocessors"
%}

@lexer lexer

expression1 -> expression1 %plus expression2
  {% postprocessWith(buildPlusNode) %}

expression1 -> expression1 %dash expression2
  {% postprocessWith(buildMinusNode) %}

expression1 -> expression2
  {% id %}


expression2 -> expression1 %times expression1
  {% postprocessWith(buildTimesNode) %}

expression2 -> expression3
  {% id %}


expression3 -> atom {% id %}

expression3 -> %dash expression3
  {% postprocessWith(buildNegateNode) %}


atom -> %parenL expression1 %parenR
  {% postprocessWith(unparenthesize) %}

atom -> number
  {% postprocessWith(buildNumLeaf) %}

atom -> %name
  {% postprocessWith(buildNameLeaf) %}

@include "./Number.ne"
