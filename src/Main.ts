import { AST, Scope, lookup } from "./AST";
import { Lexer, Token } from "moo";
import { CompiledRules, Grammar, Parser } from "nearley";
import { lexer } from "./Grammar/Lexer";
export { default as expressionRules } from "../gen/Grammar/Expression";
export { default as scopeRules } from "../gen/Grammar/Scope";

export function lex(source: string): Token[] {
  lexer.reset(source);
  return Array.from(lexer);
}

export function parse(rules: CompiledRules, source: string): AST[] {
  return new Parser(Grammar.fromCompiled(rules)).feed(source).finish();
}

export class NoParseError extends Error { }
export class AmbiguousParseError extends Error { }

export function parseUnambiguous(rules: CompiledRules, source: string): AST {
  const parses = parse(rules, source);
  if (parses.length == 1)
    return parses[0];
  else if (parses.length == 0)
    throw new NoParseError("input is invalid: " + source);
  else
    throw new AmbiguousParseError("input is ambiguous: " + source);
}

export function treeToString(tree: AST): string {
  switch (tree.tag) {
    case "plus":
      return (
        "(" + treeToString(tree.leftSubtree) +
        " + " + treeToString(tree.rightSubtree) +
        ")"
      );

    case "minus":
      return (
        "(" + treeToString(tree.leftSubtree) +
        " - " + treeToString(tree.rightSubtree) +
        ")"
      );

    case "times":
      return (
        "(" + treeToString(tree.leftSubtree) +
        " * " + treeToString(tree.rightSubtree) +
        ")"
      );

    case "exponent":
      return (
        "(" + treeToString(tree.leftSubtree) +
        " ^ " + treeToString(tree.rightSubtree) +
        ")"
      );

    case "negate":
      return "(- " + treeToString(tree.subtree) + ")";

    case "num":
      return tree.value.toString();

    case "name":
      return tree.name;
  }
}

export function interpret(scope: Scope, tree: AST): number {
  switch (tree.tag) {
    case "plus":
      return (
        interpret(scope, tree.leftSubtree) +
        interpret(scope, tree.rightSubtree)
      );

    case "minus":
      return (
        interpret(scope, tree.leftSubtree) -
        interpret(scope, tree.rightSubtree)
      );

    case "times":
      return (
        interpret(scope, tree.leftSubtree) *
        interpret(scope, tree.rightSubtree)
      );

    case "exponent":
      // ** is "to the power of" in TypeScript
      return (
        interpret(scope, tree.leftSubtree) **
        interpret(scope, tree.rightSubtree)
      );

    case "negate":
      return - interpret(scope, tree.subtree);

    case "num":
      return tree.value;

    case "name":
      return lookup(tree.name, scope);
  }
}
